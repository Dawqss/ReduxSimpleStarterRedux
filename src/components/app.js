import React, { Component } from 'react';
import BookList from '../containers/bookList';
import BookDetail from '../containers/bookDetail'
import AddBook from '../containers/addBook';

export default class App extends Component {
  render() {
    return (
      <div>
        <BookList />
        <BookDetail />
        <AddBook />
      </div>
    );
  }
}

import React, {Component} from 'react';
import { connect } from 'react-redux';
import { selectBook } from "../actions";
import { bindActionCreators } from 'redux';

class BookList extends Component {
    renderList() {
        return this.props.books.map((book) => {
            return (
                <a key={book.title}
                    onClick={() => this.props.selectBook(book)}
                    className="list-group-item list-group-item-action">
                    {book.title}
                </a>
            )
        });
    }

    render() {
        return (
            <div className="list-group col-sm-4">
                {this.renderList()}
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        books: state.books
    };
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({ selectBook: selectBook}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(BookList);
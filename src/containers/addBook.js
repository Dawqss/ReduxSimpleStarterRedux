import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addBooks } from "../actions";
import { bindActionCreators } from 'redux';




class AddBook extends Component {
    constructor(props) {
        super(props);
        this.state = {term: '', array: []}

    }

    insidePush(list, item) {
        let newArray = list.concat({title: item});
        this.props.addBook(newArray);
    }

    render() {
        return (
            <div className="pt-3">
                <div className="form-group">
                    <label>Book name</label>
                    <input type="text" onChange={event => {this.setState({term: event.target.value})}} className="form-control"/>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <a href="#"
                           onClick={() => {this.insidePush(this.props.books, this.state.term)}}
                           className="btn btn-primary">
                            Add Book
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    return {
        books: state.books
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ addBook: addBooks}, dispatch)
}


export default connect(mapStateToProps, mapDispatchToProps)(AddBook);


export function selectBook(book) {
    return {
        type: 'BOOK_SELECTED',
        payload: book
    };
}


export function addBooks(name) {
    return {
        type: 'BOOK_ADDED',
        payload: name
    }
}